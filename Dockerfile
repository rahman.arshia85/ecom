FROM python:3.8-slim
LABEL maintainer="phillipmbates85@gmail.com"
RUN mkdir -p /app
WORKDIR /app
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY /ecommerce .
EXPOSE 8000
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
#RUN python manage.py collectstatic --noinput
